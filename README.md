# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062332
- [ ] Name : 周長諭
- [ ] URL :https://shwu10.cs.nthu.edu.tw/105062332/lab-flexb-resp-quiz/blob/master/index.html

## 10 Bootstrap elements (list in here)
1. navbar nav-tabs: 用來作為索引，連結至其他網頁
2. d-flex: 建立可隨螢幕伸縮的block
3. container: 一個容器
4. flex-column: 將一個flexbox內的東西以column方式排列
5. btn btn-secondary: 建按鈕，secondary為按鈕的顏色
6. rounded-circle: 將圖片改為圓形
7. justify-content-center
8. align-items-center: 與7合併為水平置中
9. col-4: 分4格給該element
10. text-center: 文字置中
